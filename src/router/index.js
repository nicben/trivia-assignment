import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
    {
        path: '/start',
        alias: '/',
        name: 'StartPage',
        component: () => import(/* webpackChunkName: "start"*/ '../components/Start/StartPage')
    },
    {
        path: '/question',
        name: 'QuestionPage',
        component: () => import(/* webpackChunkName: "question"*/ '../components/Question/QuestionPage')
    },
    {
        path: '/result',
        name: 'ResultPage',
        component: () => import(/* webpackChunkName: "result"*/ '../components/Result/ResultPage')
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router;

