import Vue from "vue";
import Vuex from "vuex";
import { UserAPI as userAPI } from "@/api/userAPI";
import { fetchQuestions } from "@/api/questionAPI";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    score: 0,
    username: "",
    user: [],
    answers: [],
    questions: {},
    numOfQuestions: 0,
    category: "",
    difficulty: "",
    error: ""
  },
  mutations: {
    addScore: state => {
      state.score += 10;
    },
    setScore: (state, payload) => {
      state.score = payload
    },
    setUsername: (state, payload) => {
      state.username = payload;
    },
    setUser: (state, payload) => {
      state.user = payload;
    },
    addAnswer: (state, payload) => {
      state.answers.push(payload);
    },
    setAnswer: (state, payload) => {
      state.answers = payload;
    },
    setQuestion: (state, payload) => {
      state.questions = payload;
    },
    setNumOfQuestions: (state, payload) => {
      state.numOfQuestions = payload;
    },
    setCategory: (state, payload) => {
      state.category = payload;
    },
    setDifficulty: (state, payload) => {
      state.difficulty = payload;
    },
    setError: (state, payload) => {
      state.error = payload;
    },
    setHighScore: (state, payload) => {
      state.user.highScore = payload;
    }
  },
  getters: {
    getNumOfQuestions: state => {
      return state.numOfQuestions;
    },
    getCategory: state => {
      return state.category;
    },
    getDifficulty: state => {
      return state.difficulty;
    },
    getAnsweredQuestions: state => {
      return state.questions;
    },
    getUserAnswers: state => {
      return state.answers;
    },
    getUsername: state => {
      return state.username;
    },
    getScore: state => {
      return state.score;
    },
    getUser: state => {
      return state.user;
    }
  },
  actions: {
    async fetchUser({ state, commit }, username) {
      try {
        const registerDetails = JSON.stringify({
          username: state.username,
          highScore: 0
        });
        const user = await userAPI.getUser(username);
        if (user.length) {
          commit("setUser", user);
          commit("setError", "");
        } else {
          commit("setError", "User was not found");
          const user = await userAPI.registerUser(registerDetails);
          if (user) {
            commit("setUser", user);
            commit("setError", "");
          } else {
            commit("setError", "User was not registered");
          }
        }
      } catch (error) {
        commit("setError", error.message);
      }
    },
    async updateHighScore({ state, commit }) {
      try {
        const highScore = JSON.stringify({
          highScore: state.score
        });
        const user = await userAPI.updateHighScore(state.user.id, highScore);
        if (!user) {
          commit("setError", "User was not found");
        }
      } catch (error) {
        commit("setError", error.message);
      }
    },
    async fetchQuestions({ state, commit }) {
      try {
        const number = JSON.stringify({
          number: state.numOfQuestions
        });
        const questions = await fetchQuestions(number);
        commit("setQuestion", questions);
      } catch (error) {
        commit("setError", error.message);
      }
    },
    async addAnswer({ commit }, answer) {
      return new Promise(resolve => {
        commit("addAnswer", answer);
        resolve();
      });
    },
    async setQuestion({ commit }, question) {
      return new Promise(resolve => {
        commit("setQuestion", question);
        resolve();
      });
    },
    async addScore({ commit }) {
      return new Promise(resolve => {
        commit("addScore");
        resolve();
      });
    }
  }
});
