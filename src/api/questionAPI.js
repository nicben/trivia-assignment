export const fetchQuestions = async (num, category, difficulty) => {
  try {
    const { results } = await fetch(
      `https://opentdb.com/api.php?amount=${num}&category=${category}&difficulty=${difficulty}`
    ).then(r => r.json());
    return [null, results];
  } catch (e) {
    return [e.message, []];
  }
};
