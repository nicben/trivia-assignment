export const fetchCategory = async () => {
    try {
        const {trivia_categories} = await fetch('https://opentdb.com/api_category.php')
            .then(r => r.json())
        return [null, trivia_categories]
    } catch (e) {
        return [e.message, []]
    }
}
