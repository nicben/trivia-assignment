//const apiNicole = "https://trivia-assignment-api.herokuapp.com";
const apiCamilla = "https://noroff-assignment-test-api.herokuapp.com"
const apiKey = "token";

export const UserAPI = {
  getUser(username) {
    return fetch(`${apiCamilla}/trivia?username=${username}`)
      .then(response => response.json())
      .then(function(result) {
        return result;
      });
  },
  registerUser(registerDetails) {
    const requestOptions = {
      method: "POST",
      headers: {
        "X-API-Key": apiKey,
        "Content-Type": "application/json"
      },
      body: registerDetails
    };
    return fetch(`${apiCamilla}/trivia`, requestOptions).then(response =>
      response.json()
    );
  },
  updateHighScore(userId, highScore) {
    const requestOptions = {
      method: "PATCH",
      headers: {
        "X-API-Key": apiKey,
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        highScore: highScore
      })
    };
    return fetch(`${apiCamilla}/trivia/${userId}`, requestOptions);
  }
};
