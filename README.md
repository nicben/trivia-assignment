# Assignment 2 - Trivia Game


## Deployment:
Deployed on Heroku

[Nicole's](https://enigmatic-temple-91316.herokuapp.com/) - URL:  <https://enigmatic-temple-91316.herokuapp.com/>

[Camilla's](https://secure-chamber-67897.herokuapp.com/) - URL:  <https://secure-chamber-67897.herokuapp.com/>

## Source:

<https://https://gitlab.com/nicben/trivia-assignment/>


## Component Tree

Link to [component tree](https://gitlab.com/nicben/trivia-assignment/-/blob/master/assets/component-tree.pdf)
